import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";

import { AuthContext } from "../context/Auth"

const Login = () => {
  const { setUser } = useContext(AuthContext);
  const navigate = useNavigate();
  const handleLogin = () => {
    navigate("/home");
    window.localStorage.setItem(
      "token",
      "qewqewqretrteruyouypu.askjdhsjah.lasjdh"
    );
    setUser(true);
  };

  return (
    <div>
      <h1>Login ---</h1>
      <button
        onClick={handleLogin}
      >
        Login in
      </button>
    </div>
  );
};

export default Login;
