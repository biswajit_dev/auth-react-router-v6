import React, { useContext } from "react";
import { useNavigate, Link } from "react-router-dom";
import { AuthContext } from "../context/Auth";

const Home = () => {
  const {setUser} = useContext(AuthContext)
  const navigate = useNavigate();
  const handleLogout = () => {
    navigate("login");
    window.localStorage.removeItem("token");
    setUser(false)
  };

  return (
    <div>
      <Link to={"/profile"}>Profile</Link>
      <h1>Home Page</h1>
      <button
        onClick={handleLogout}
      >
        Logout
      </button>
    </div>
  );
};

export default Home;
