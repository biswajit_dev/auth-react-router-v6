import React, { useEffect, useState, useContext } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Profile from "./pages/Profile";

import { AuthContext } from "./context/Auth";

function App() {
  const [token, setToken] = useState(null);
  const { user, setUser } = useContext(AuthContext);

  useEffect(() => {
    setToken(window.localStorage.getItem("token"));
  }, [user]);

  useEffect(()=> {
    if(token) setUser(true);
  }, [token]);

  return (
    <div className="App">
      <Router>
        <Routes>
          {!token ? (
            <Route path="/login" element={<Login />} />
          ) : (
            <>
              <Route path="/home" element={<Home />} />
              <Route path="/profile" element={<Profile />} />
            </>
          )}
          <Route
            path="*"
            element={
              token ? <Navigate to={"/home"} /> : <Navigate to={"/login"} />
            }
          />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
